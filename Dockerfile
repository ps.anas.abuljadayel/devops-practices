FROM openjdk:8-jdk-alpine

WORKDIR /
COPY artifacts/assignment-*.jar assignment.jar
ENV spring.profiles.active=h2
ENV server.port=8070
EXPOSE 8070
ENTRYPOINT ["java","-jar","/assignment.jar"]
